# Project 02: Process Queue Shell

This is [Project 02] of [CSE.30341.FA19].

## Student

- Domer McDomerson (dmcdomers@nd.edu)

## Brainstorming

The following are questions that should help you in thinking about how to
approach implementing [Project 02].  For this project, responses to these
brainstorming questions are **not required**.

### Process

1. Given a command string such as `sleep 10`, how would start a process that
   executes this command?  In particular, how would you handle the name of the
   program (ie. `sleep`) versus the arguments to the program (ie. `10`)?

2. What signals do you need to send to a process to **pause** it (ie. suspend
   its execution) or to **resume** it (ie. continue its execution)?

### Queue

1. What must happen when **pushing** a process to a `Queue`?

2. What must happen when **popping** a process from a `Queue`?

3. What must happen when **removing** a process from a `Queue`?

### Shell

1. What **system call** will allow us to trigger a timer event at periodic
   intervals?  What signal does it send when a timer is triggered?

2. What functions would allow you to parse strings with various arguments?

### Scheduler

1. What needs to be created when calling `scheduler_add` and where should this
   object go?

2. How would you determine if you should display a particular queue in the
   `scheduler_status` function?
   
3. How would you wait for a process without blocking? What information do you
   need to update when a process terminates?

### Signal

1. What should happen in the `Scheduler` when a timer event fires?

### Timestamp

1. How would you the result of `gettimeofday` to return a `double` representing
   the current time in **seconds**?

## Reflection

Once you have completed your project, you are to answer the following
**reflection** questions:

1. Compare and constrast the **fifo** and **round robin** process scheduling
   policies.  In particular, explain how they optimize for either
   **turnaround** or **response** time.

    > RESPONSE REQUIRED (limit to a **few sentences**).

2. Describe how a **multi-level feedback queue** attempts to combine the best
   aspects of **fifo** and **round robin**.  In particular, explain how **I/O**
   impacts the priority of a process in this system.

    > RESPONSE REQUIRED (limit to a **few sentences**).

3. Consider a single-CPU, a timeslice of `10` ms, and the following processes:

    ```
    Process       Arrival Time        Run Time
    A             0 ms                20 ms
    B             10 ms               30 ms
    C             20 ms               10 ms
    ```

    - Sketch out exactly when each process runs, and for how long given the
      **FIFO** scheduling policy.

        ```
        Waiting:
        Running:
        Time:       0       10      20      30      40      50      60
        ```

    - Sketch out exactly when each process runs, and for how long given the
      **RDRN** scheduling policy.

        ```
        Waiting:
        Running:
        Time:       0       10      20      30      40      50      60
        ```

        **Note**: At the beginning of each timeslice, push any new processes to
        the back of the waiting queue **before** rotating processes.

    - Sketch out exactly when each process runs, and for how long given the
      **MLFQ** scheduling policy. You may assume that `Q0` has a timeslice of
      `10` ms, Q1 has a timeslice of `20` ms, and `Q2` has a timeslice of `40`
      ms.

        ```
        Queue 0:
        Queue 1:
        Queue 2:
        Running:
        Time:       0       10      20      30      40      50      60
        ```

        **Note**: You should record the processes in the priority levels `Q0`,
        `Q1`, and `Q2` in addition to sketching which process is running at the
        particular time step.

## Errata

> Describe any known errors, bugs, or deviations from the requirements.

## Extra Credit

> Describe what extra credit (if any) that you implemented.

[Project 02]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa19/project02.html
[CSE.30341.FA19]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa19/
